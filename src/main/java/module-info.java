module example.javafxcssdemo {
        requires javafx.controls;
        requires javafx.fxml;
        requires javafx.web;
        requires java.desktop;


      //  requires org.kordamp.bootstrapfx.core;

        opens example.javafxcssdemo to javafx.fxml;
        exports example.javafxcssdemo;

}